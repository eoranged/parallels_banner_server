# -*- coding: utf-8 -*-
"""
Banner selection logic.
"""
from collections import defaultdict
import random

class Banner(object):
    """Banner representation"""
    def __init__(self, shows, url, categories):
        self._shows = int(shows)
        self._shows_left = int(shows)
        self._url = url
        self._categories = categories

    @property
    def shows_total(self):
        return self._shows

    @property
    def shows_left(self):
        return self._shows_left

    @property
    def url(self):
        return self._url

    @property
    def categories(self):
        return self._categories

    def record_impression(self):
        self._shows_left -= 1


class BannerData(object):
    """
    Banner display configuration
    """
    def __init__(self, data):
        banners_by_category = defaultdict(list)
        for row in data:
            url, shows, categories = row
            banner = Banner(shows, url, categories)
            for category in categories:
                banners_by_category[category].append(banner)
        self._banners_by_category = banners_by_category

    def _get_by_categories(self, categories):
        result_set = set()
        if not categories:
            categories = self._banners_by_category.keys()
        for category in categories:
            banners = self._banners_by_category[category]
            result_set.update(banners)
        return list(result_set)

    def get_banner(self, categories):
        """
        Returns banner matching specified categories.

        Verifies whether banner have paid impressions left and updates the counter.
        If no impressions are left returns None.
        Attempts to decrease number of cases when all banners in category have
        zero impressions left.
        """
        banners = self._get_by_categories(categories)
        weighted_list = []
        upper_boundary = 0
        for banner in banners:
            if not banner.shows_left:
                continue
            upper_boundary += banner.shows_left
            weighted_list.append((banner.shows_left, banner))
        # If there are no banners to show, return None
        if not weighted_list:
            return None
        selected_weight = random.randint(0, upper_boundary-1)
        # Can be improved by performing analysis of impessions left in all categories
        # and ensuring minimal impact on banners in category with less impressions
        # Can be optimized by utilizing binary search
        for weight, banner in weighted_list:
            selected_weight -= weight
            if selected_weight < 0:
                banner.record_impression()
                return banner
