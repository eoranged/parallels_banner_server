# -*- coding: utf-8 -*-
"""
HTTP server component.
"""
import BaseHTTPServer
import urlparse

import logic


class BannerHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    """
    Request handler.
    """
    # Making it "global" because we've got only one server instance here
    # and dealing with class-level variable is quite easier in this case
    banners = None

    def do_GET(self):
        """Respond to a GET request."""
        path = self.path
        categories = []
        if '?' in path:
            path, args = path.split('?', 1)
            qs = urlparse.parse_qsl(args)
            categories = [pair[1] for pair in qs if pair[0] == "category[]"]
        banner = self.banners.get_banner(categories)
        url = banner.url if banner else None
        self.send_banner(url)

    def send_banner(self, url):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write("<html><head><title></title></head>")
        self.wfile.write("<body>")
        if url:
            self.wfile.write('<img src="{url}"></img>'.format(url=url))
        #else:
        #    self.wfile.write('<img src="http://banners.com/placeholder.png"></img>'.format(url))
        self.wfile.write("</body></html>")


def start_server(banners, host="0.0.0.0", port=8000):
    """Start Banner serving HTTP server."""
    server_class = BaseHTTPServer.HTTPServer
    BannerHandler.banners = banners
    httpd = server_class((host, port), BannerHandler)
    print("Starting HTTP server on {host}:{port}".format(host=host, port=port))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
