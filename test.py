import unittest

import main
import logic


class TestLogic(unittest.TestCase):

    def setUp(self):
        data = main.load_file("test_data.csv")
        self.banners = logic.BannerData(data)

    def tearDown(self):
        # cleaning up
        self.banners = None

    def test_no_categories(self):
        banner = self.banners.get_banner([])
        self.check_banner(banner)

    def test_single_category(self):
        banner = self.banners.get_banner(["benny hill"])
        self.check_banner(banner)
        self.assertIn("benny hill", banner.categories)

    def test_multiple_categories(self):
        banner = self.banners.get_banner(["sandbox", "flight"])
        self.check_banner(banner)
        category_correct = "sandbox" in banner.categories or "flight" in banner.categories
        self.assertTrue(category_correct, 'Banner categories should contain flight or sandbox')
    def test_exact(self):
        banner = self.banners.get_banner(["britain"])
        self.check_banner(banner)
        self.assertEquals(banner.url, "http://banners.com/banner2.jpg", "Expected to get exact banner for britain category")

    def check_banner(self, banner):
        self.assertIsNotNone(banner, "No banner")
        url = getattr(banner, "url", None)
        self.assertIsNotNone(url, "Banner has no url")
        self.assertIsInstance(url, str, "Banner url should be string")
        self.assertTrue(url.startswith("http://") or url.startswith("https://"),
                          "Banner url should start with http:// or https://")



if __name__ == '__main__':
    unittest.main()
