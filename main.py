#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import argparse
import csv

import server
import logic


def load_file(csv_file):
    """
    Loading banner info.

    Assuming here that the file exists and correct.
    1000 records fit well in memory, so everything fits in memory just fine.

    :param csv_file: csv file name
    """
    data = []
    with open(csv_file) as ifile:
        reader = csv.reader(ifile, delimiter=";")
        for row in reader:
            url = row[0]
            shows_left = row[1]
            categories = row[2:]
            data.append((url, shows_left, categories))
    return data


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("csv_file", help="CSV file with banner data")
    parser.add_argument("--host", help="Host to listen for incoming connections",
                        default="0.0.0.0")
    parser.add_argument("--port", help="Port to listen", type=int, default=8000)
    args = parser.parse_args()

    data = load_file(args.csv_file)
    data = logic.BannerData(data)

    server.start_server(data, args.host, args.port)


if __name__ == "__main__":
    main()
